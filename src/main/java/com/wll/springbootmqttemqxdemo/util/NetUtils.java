package com.wll.springbootmqttemqxdemo.util;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author wanglulu
 * @date 2019/11/15$ 15:55$
 */
public class NetUtils {


    public NetUtils() {

    }


    /**
     * @methendName: connectTest
     * @description: 测试连接
     * @param: address
     * @return: boolean
     */
    public static boolean connectTest(String address) {
        boolean result = false;

        try {
            URL url = new URL(address);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("HEAD");
            conn.setReadTimeout(3000);
            conn.connect();
            if (conn.getResponseCode() == 200) {
                result = true;
            }

            conn.disconnect();
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        return result;
    }
}
