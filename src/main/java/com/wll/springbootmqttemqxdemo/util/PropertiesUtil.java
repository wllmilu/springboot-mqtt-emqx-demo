package com.wll.springbootmqttemqxdemo.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author wanglulu
 * @date 2019/11/14$ 15:56$
 */
public class PropertiesUtil {

    public static String MQTT_HOST;
    public static String MQTT_CLIENTID;
    public static String MQTT_USER_NAME;
    public static String MQTT_PASSWORD;
    public static int MQTT_TIMEOUT;
    public static int MQTT_KEEP_ALIVE;
    public static String prefixUrl;
    /**
     * 最大重连次数
     */
    public static int MQTT_MAXRECONNECTTIMES;
    public static int MQTT_RECONNINTERVAL;


    static {
        Properties properties = loadMqttProperties();
        MQTT_HOST = properties.getProperty("host");
        MQTT_CLIENTID = properties.getProperty("clientId");
        MQTT_USER_NAME = properties.getProperty("username");
        MQTT_PASSWORD = properties.getProperty("password");
        MQTT_TIMEOUT = Integer.valueOf(properties.getProperty("timeout"));
        MQTT_KEEP_ALIVE = Integer.valueOf(properties.getProperty("keepalive"));
        MQTT_MAXRECONNECTTIMES = Integer.valueOf(properties.getProperty("maxReconnectTimes"));
        MQTT_RECONNINTERVAL = Integer.valueOf(properties.getProperty("reconnInterval"));
        prefixUrl = String.valueOf(properties.getProperty("prefixUrl"));
    }

    /**
     * 获取配置文件中的属性值
     * @return
     */
    private static Properties loadMqttProperties() {
        InputStream inputstream = PropertiesUtil.class.getResourceAsStream("/application.yml");
        Properties properties = new Properties();
        try {
            properties.load(inputstream);
            return properties;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (inputstream != null) {
                    inputstream.close();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }


}
