package com.wll.springbootmqttemqxdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMqttEmqxDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMqttEmqxDemoApplication.class, args);
    }

}
