//package com.wll.springbootmqttemqxdemo.me.config;
//
//
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Configuration;
//
//
//@Configuration
//@ConfigurationProperties("mqtt")
//public class MqttConfiguration {
//
//    public String host;
//    public String clientId;
//    public String username;
//    public String password;
//    public String topic;
//    public int timeout;
//    public int keepalive;
//
//    public String getHost() {
//        return host;
//    }
//
//    public void setHost(String host) {
//        this.host = host;
//    }
//
//    public String getClientId() {
//        return clientId;
//    }
//
//    public void setClientId(String clientId) {
//        this.clientId = clientId;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public String getTopic() {
//        return topic;
//    }
//
//    public void setTopic(String topic) {
//        this.topic = topic;
//    }
//
//    public int getTimeout() {
//        return timeout;
//    }
//
//    public void setTimeout(int timeout) {
//        this.timeout = timeout;
//    }
//
//    public int getKeepalive() {
//        return keepalive;
//    }
//
//    public void setKeepalive(int keepalive) {
//        this.keepalive = keepalive;
//    }
//
//
////    @Bean
////    public MqttPushClient getMqttPushClient() {
////        mqttPushClient.connect(host, clientId, username, password, timeout, keepalive);
////        // 以/#结尾表示订阅所有以test开头的主题
////        mqttPushClient.subscribe("test/#", 0);
////        return mqttPushClient;
////    }
//}
