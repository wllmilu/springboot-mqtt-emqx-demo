package com.wll.springbootmqttemqxdemo.me.controller;

import com.wll.springbootmqttemqxdemo.me.config.MqttPushClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test/")
public class IndexTestController {

    @Autowired
    public MqttPushClient pushClient;

    /**
     * 发布消息
     * @param topic
     * @param pushMessage
     */
    @RequestMapping("push")
    public void push(String topic, String pushMessage){
        System.out.println("发送: topic: "+ topic + ", pushMessage: " + pushMessage);
        pushClient.pushlish(topic, pushMessage);
    }


    /**
     * 消费消息
     * @param topic
     */
    @RequestMapping("consumption")
    public void consumption(String topic, String pushMessage){
        System.out.println("消费: topic: "+ topic);
        pushClient.subscribe(topic);
    }


}
