# springboot mqqt emqx 整合demo

# emqx 

MQTT 官网： https://www.emqx.com/zh

安装教程： https://blog.csdn.net/qq_38113006/article/details/105655753

下载地址： https://www.emqx.com/zh/downloads-and-install?product=broker&version=4.3.11&os=Windows&oslabel=Windows


![在这里插入图片描述](https://img-blog.csdnimg.cn/20200421154007889.png)

这是因为没有安装如下软件导致的，点击如下链接，下载vcredist并安装：https://www.microsoft.com/zh-CN/download/details.aspx?id=40784

进入 bin目录下 打开cmd窗口

启动  emqx start

停止  emqx stop

访问：http://127.0.0.1:18083

username： admin

password：public

###当我们启动了EMQ之后就可以使用客户端进链接了,端口如下

1883 MQTT 协议端口

8883 MQTT/SSL 端口

8083 MQTT/WebSocket 端口

8080 HTTP API 端口

18083 Dashboard 管理控制台端口通过IP访问18083端口可以通过Dashboard在线观察EMQ的运行状态等参数默认用户: admin，密码：public (可在平台中配置用户)



# 搭建参考地址
https://www.cnblogs.com/Forever-wind/p/14894597.html








